# demo-cicd-flask

Python Flask Demo with Gitlab CI/CD

## Resources:

- https://medium.com/python-pandemonium/build-simple-restful-api-with-python-and-flask-part-2-724ebf04d12
- https://medium.freecodecamp.org/structuring-a-flask-restplus-web-service-for-production-builds-c2ec676de563
- https://medium.com/@camillovisini/barebone-flask-rest-api-ac263db82e40
- https://pythonprogramming.net/password-hashing-flask-tutorial/

