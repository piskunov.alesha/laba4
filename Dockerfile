FROM python:3.7

COPY requirements.txt /

RUN pip3 install --upgrade pip
RUN pip3 install -r requirements.txt
RUN apt-get update -qq

